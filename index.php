<?php

    function CallAPI($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }
    var_dump($_SERVER);
    $resultado = CallAPI("POST", "https://reqbin.com/echo/post/json");
    $r = json_decode($resultado);

    if($r->success){    
        $token = '60bf6262c498481f0df1a9dd';
        echo '<iframe width="1000" height="760" src="https://beta.eggeducacion.com/team/60bf625b3b7d891e794f699b/subject/'.$token.'/"></iframe>';
    }

?>